# Main module to interact with Marvel APIs
module MarvelFetcher
  # request marvel and get the story of requested character
  # @param favourite character [String]
  # @return [MarvelStory]
  def self.fetch_favorite_char_story(favourite_char)
    marvel_client.character_stories(favourite_char, limit: 1, offset: 1000)

  rescue Exception => e
    Rails.logger.info "Something went wrong: #{e}"
  end

  # request marvel and get the characters of the story
  # @param story_id [Integer]
  # @return [characters]
  def self.fetch_story_chars(story_id)
    marvel_client.story_characters(story_id)
  rescue Exception => e
    Rails.logger.info "Something went wrong: #{e}"
  end

  # Initialize marvel client
  # @return marvel_client
  def self.marvel_client
    @marvel_client ||= Marvelite::API::Client.new(
      public_key: Figaro.env.marvel_public_key,
      private_key: Figaro.env.marvel_private_key
    )
  end
end
