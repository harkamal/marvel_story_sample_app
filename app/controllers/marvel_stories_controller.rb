class MarvelStoriesController < ApplicationController
  FAVORITE_CHARACTER = 'Spider-man'.freeze
  respond_to :json, :xml, :html

  # GET /marvel_story
  # @param character [String]
  # @return [marvel_story, characters]
  def index
    @character = params[:character].blank? ? FAVORITE_CHARACTER : params[:character].strip
    @marvel_story = MarvelFetcher.fetch_favorite_char_story @character


    @characters = MarvelFetcher.fetch_story_chars(@marvel_story['data']['results'][0]['id']) rescue nil 

    respond_with @marvel_story, @characters
  end
end
