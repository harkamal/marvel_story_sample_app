module MarvelStoriesHelper
  def invalid_marvel_story_response
    @marvel_story['status'] != 'Ok'
  end

  def count_greater_than_zero
    @marvel_story["data"]["count"] > 0 rescue false
  end

  def marvel_story_result
    @marvel_story_result ||= begin
      @marvel_story['data']['results'][0]
    rescue
      nil
    end
  end

  def marvel_story_characters
    @story_characters ||= begin
      @characters['data']['results']
    rescue
      nil
    end
  end

  def char_image_url(character)
    "#{character['thumbnail']['path']}/portrait_small.#{character['thumbnail']['extension']}"
  end
end
