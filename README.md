
## This is a sample application developed in Ruby on Rails which displays a marvel story. We can also search for a marvel story. Search term should be a valid marvel character.

## Application have one view where you can enter the marvel character name and displays the corresondig marvel story randomized to a offset value 1000.  machine:

```no-highlight
git clone https://gitlab.com/harkamal/marvel_story_sample_app.git
cd marvel_story_sample_app/
bundle install

cp config/database.sample.yml config/database.yml
bundle exec rake db:create db:migrate
bundle exec rake db:test:prepare
bundle exec rails s
open localhost:3000
```

```no-highlight
Demo: https://enigmatic-everglades-94233.herokuapp.com
```
